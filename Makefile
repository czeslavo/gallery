all:
	@echo "make [static/dev/test]"

static:
	./_misc/build_static.sh

dev:
	./_misc/run_dev.sh

test:
	./manage.py test
