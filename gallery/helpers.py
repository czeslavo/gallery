import math
import datetime
import pytz


# Earth radius (EPSG:3857).
earth_radius = 6378137

# Where the top left corner of the tile (0, 0) starts (EPSG:3857).
mercator_origin = (-20037508.342789244, 20037508.342789244)

# Size of the world (EPSG:3857).
mercator_world_size = (mercator_origin[1] * 2, mercator_origin[1] * 2)


def utc_now():
    """Returns an aware datetime object representing the current time in UTC."""
    return pytz.utc.localize(datetime.datetime.utcnow())


def get_user_location(request):
    """If the user is authenticated and has set his location in his profile that
    location is returned. Otherwise a predicted location is returned or a
    preselected location if it can't be detemined at all.

    request: An HttpRequest object.
    """
    if request.user.is_authenticated() and \
       request.user.userdata.location_is_set():
        return {
            'lat': request.user.userdata.lat,
            'lon': request.user.userdata.lon,
        }
    return {
        'lat': 50.061389,
        'lon': 19.938333,
    }


def coords_to_mercator(lon, lat):
    """Converts EPSG:4326 coords to EPSG:3857."""
    if abs(lat) > 85:
        raise ValueError('Mercator does not support latitudes above ~85 degrees')
    y = earth_radius * math.log(math.tan(math.pi / 4 + math.radians(lat) / 2))
    x = earth_radius * math.radians(lon)
    return x, y


def mercator_to_coords(x, y):
    """Converts EPSG:3857 coords to EPSG:4326."""
    lat = 2 * (math.atan(math.pow(math.e, y / earth_radius)) - math.pi / 4)
    lon = x / earth_radius
    return math.degrees(lon), math.degrees(lat)


def mercator_to_tile(x, y, n):
    """Converts EPSG:3857 coordinates to a tile number. [0, 0] is the top left
    tile and [n - 1, n - 1] is the bottom right tile.

    x, y: EPSG:3857 coordinates.
    n: number of tiles per map width or height (for example n=16 means that
       there are 256 tiles in total).
    """
    origin = [-20037508.342789244, 20037508.342789244]
    tile_size = 2 * origin[1] / n
    x -= origin[0]
    y -= origin[1]
    tileX = x / tile_size
    tileY = y / tile_size
    return math.floor(tileX), math.floor(-tileY)


def bearing(lon1, lat1, lon2, lat2):
    """Calculates an initial bearing in degrees between two coordinates."""
    lon1 = math.radians(lon1)
    lat1 = math.radians(lat1)
    lon2 = math.radians(lon2)
    lat2 = math.radians(lat2)

    y = math.sin(lon2 - lon1) * math.cos(lat2)
    x = math.cos(lat1) * math.sin(lat2) - \
        math.sin(lat1) * math.cos(lat2) * math.cos(lon2 - lon1)
    bearing = math.atan2(y, x)
    return math.degrees(bearing)


def distance(lon1, lat1, lon2, lat2):
    """Calculates a distance in kilometers between two coordinates."""
    lon1 = math.radians(lon1)
    lat1 = math.radians(lat1)
    lon2 = math.radians(lon2)
    lat2 = math.radians(lat2)

    p1 = pow(math.sin((lat2 - lat1) / 2), 2)
    p2 = pow(math.sin((lon2 - lon1) / 2), 2)
    a =  p1 + math.cos(lat1) * math.cos(lat2) * p2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    return c * 6371


def get_number_of_tiles(zoom):
    """Returns the number of tiles per map width or height at the specified zoom
    level.
    """
    return math.pow(2, zoom)


def get_tile_extent(x, y, z):
    """Returns the extent of the given tile. It can be used to check if a point
    lies within a certain tile.
    """
    n = get_number_of_tiles(z)
    tile_size = (mercator_world_size[0] / n, mercator_world_size[1] / n)
    ax = mercator_origin[0] + tile_size[0] * x
    ay = mercator_origin[1] + tile_size[1] * -(y+1)
    bx = mercator_origin[0] + tile_size[0] * (x+1)
    by = mercator_origin[1] + tile_size[1] * -(y)
    return ax, ay, bx, by


def get_lat_offset(distance):
    """Converts distance to latitude degrees. This function provides only rough
    estimation and should be used only for initial filtering when selecting
    objects from the database.

    distance: [km]
    """
    return distance / 110.54


def get_lon_offset(distance, latitude):
    """Converts distance to longitude degrees. This function provides only rough
    estimation and should be used only for initial filtering when selecting
    objects from the database.

    distance: [km]
    """
    return distance / 111.320 * math.cos(math.radians(latitude))
