from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import CurrentPhoto, Photo, User, UserData


class UserDataInline(admin.StackedInline):
    model = UserData
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (UserDataInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Photo)
admin.site.register(CurrentPhoto)
