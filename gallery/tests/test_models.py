from django.test import TestCase
from ..models import UserData


class UserDataTestCase(TestCase):

    def test_valid_location(self):
        ud = UserData(lat=0, lon=0, display_location=True)
        self.assertTrue(ud.should_display_location())
        self.assertTrue(ud.location_is_set())

    def test_missing_lat(self):
        ud = UserData(lon=0, display_location=True)
        self.assertFalse(ud.should_display_location())
        self.assertFalse(ud.location_is_set())

    def test_missing_lon(self):
        ud = UserData(lat=0, display_location=True)
        self.assertFalse(ud.should_display_location())
        self.assertFalse(ud.location_is_set())

    def test_dont_display(self):
        ud = UserData(lat=0, lon=0)
        self.assertFalse(ud.should_display_location())
        self.assertTrue(ud.location_is_set())
