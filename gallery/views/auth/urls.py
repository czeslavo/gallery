from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.core.urlresolvers import reverse_lazy
from . import views


urlpatterns = [
    url(r'^register/$', views.RegisterView.as_view(), name='register'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': reverse_lazy('gallery:core:index')}, name='logout'),
    url(r'^settings/profile/$', views.EditProfileView.as_view(), name='edit_profile'),
    url(r'^settings/account/$', views.EditAccountView.as_view(), name='edit_account'),
]
