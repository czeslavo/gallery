from django.core.paginator import Paginator, Page, EmptyPage, PageNotAnInteger


class CustomPage(Page):

    def iter_pages(self):
        min_page = max(1, self.number - self.paginator.pages_offset)
        max_page = min(self.paginator.num_pages, min_page + 2 * self.paginator.pages_offset)
        min_page = max(1, max_page - 2 * self.paginator.pages_offset)
        for i in range(min_page, max_page + 1):
            yield i


class CustomPaginator(Paginator):

    def __init__(self, *args, pages_offset=3, **kwargs):
        self.pages_offset = pages_offset
        super(CustomPaginator, self).__init__(*args, **kwargs)

    def _get_page(self, *args, **kwargs):
        return CustomPage(*args, **kwargs)


def paginate(objects, objects_per_page, page):
    paginator = CustomPaginator(objects, objects_per_page)
    try:
        return paginator.page(page)
    except PageNotAnInteger:
        return paginator.page(1)
    except EmptyPage:
        return paginator.page(paginator.num_pages)
