from django.db.models import Count
from django.utils.translation import ugettext as _
from ...forms.core import AddCurrentPhotoForm
from ...models import CurrentPhoto, Photo, PhotoVote, CurrentPhotoVote
from ... import helpers
from ..mixins import LoginRequiredMixin
from .base import APIView
from .errors import BadRequestAPIError, UnauthorizedAPIError


class AddCurrentPhotoView(LoginRequiredMixin, APIView):

    def handle_form(self, form):
        # Confirm that the photo exists
        photo = Photo.objects.filter(pk=self.kwargs['pk']).first()
        if photo is None:
            raise BadRequestAPIError(message=_('The historical photo does not exist.'))

        # Confirm that the current photo does not exist
        current_photo = CurrentPhoto.objects.filter(user=self.request.user,
                                                    photo=photo).first()
        if current_photo is not None:
            raise BadRequestAPIError(message=_('You have already uploaded a current photo of this location.'))

        if not form.is_valid():
            raise BadRequestAPIError(message=_('Invalid image.'))

        current_photo = form.save(commit=False)

        # Confirm that the aspect ratio is correct
        photo_ratio = photo.image.width / photo.image.height
        current_photo_ratio = current_photo.image.width / current_photo.image.height
        if abs(1 - photo_ratio/current_photo_ratio) > 0.005:
            raise BadRequestAPIError(message=_('The uploaded image has an invalid aspect ratio.'))

        current_photo.user = self.request.user
        current_photo.photo = photo
        current_photo.save()
        return current_photo

    def post(self, request, *args, **kwargs):
        form = AddCurrentPhotoForm(request.POST, request.FILES)
        current_photo = self.handle_form(form)
        return current_photo.json()


class PhotoVoteView(APIView):

    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            raise UnauthorizedAPIError

        if not 'upvote' in self.request.POST or not 'id' in self.request.POST:
            raise BadRequestAPIError(message=_('Parameter missing.'))

        photo = Photo.objects.filter(pk=self.request.POST['id']).first()
        if photo is None:
            raise BadRequestAPIError(message=_('The historical photo does not exist.'))

        if self.request.POST['upvote'] == 'true':
            if not PhotoVote.objects.filter(user=self.request.user, photo=photo).exists():
                pv = PhotoVote(user=self.request.user, photo=photo)
                pv.save()
            return {'upvoted': True}
        else:
            pv = PhotoVote.objects.filter(user=self.request.user, photo=photo).delete()
            return {'upvoted': False}


class CurrentPhotoVoteView(APIView):

    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            raise UnauthorizedAPIError

        if not 'upvote' in self.request.POST or not 'id' in self.request.POST:
            raise BadRequestAPIError(message=_('Parameter missing.'))

        current_photo = CurrentPhoto.objects.filter(pk=self.request.POST['id']).first()
        if current_photo is None:
            raise BadRequestAPIError(message=_('The photo does not exist.'))

        if self.request.POST['upvote'] == 'true':
            if not CurrentPhotoVote.objects \
                .filter(user=self.request.user, current_photo=current_photo) \
                .exists():
                pv = CurrentPhotoVote(user=self.request.user, current_photo=current_photo)
                pv.save()
            return {'upvoted': True}
        else:
            pv = CurrentPhotoVote.objects \
                .filter(user=self.request.user, current_photo=current_photo) \
                .delete()
            return {'upvoted': False}


class PhotosTilesView(APIView):

    def get(self, request, *args, **kwargs):
        x = int(self.kwargs['x'])
        y = int(self.kwargs['y'])
        z = int(self.kwargs['z'])

        n = helpers.get_number_of_tiles(z)
        if z < 0 or x < 0 or y < 0 or x >= n or y >= n:
            raise BadRequestAPIError(message=_('Invalid tile coords.'))

        x1, y1, x2, y2 = helpers.get_tile_extent(x, y, z)
        lon1, lat1 = helpers.mercator_to_coords(x1, y1)
        lon2, lat2 = helpers.mercator_to_coords(x2, y2)
        q = Photo.objects \
            .filter(lon__gt=lon1, lat__gt=lat1, lon__lte=lon2, lat__lte=lat2) \
            .annotate(num_current_photos=Count('currentphoto')) \
            .all()
        photos = []
        for photo in q:
            photos.append({
                'id': photo.pk,
                'url': photo.get_absolute_url(),
                'lat': photo.lat,
                'lon': photo.lon,
                'current_photos': photo.num_current_photos,
            })
        return {
            'photos': photos
        }
