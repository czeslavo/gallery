from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^add/(?P<pk>([0-9]+))/$', views.AddCurrentPhotoView.as_view(), name='add_current_photo'),
    url(r'^vote/photo/$', views.PhotoVoteView.as_view(), name='vote_photo'),
    url(r'^vote/current_photo/$', views.CurrentPhotoVoteView.as_view(), name='vote_current_photo'),
    url(r'^photos/(?P<x>([0-9]+))/(?P<y>([0-9]+))/(?P<z>([0-9]+))/tile.json$', views.PhotosTilesView.as_view(), name='photos_tiles'),
]
