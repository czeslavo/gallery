from django.utils.translation import ugettext as _


class APIError(Exception):
    status_code = 500
    error_code = 'unknown'
    message = _('Unknown server error.')

    def __init__(self, status_code=None, error_code=None, message=None):
        if status_code is not None:
            self.status_code = status_code
        if error_code is not None:
            self.error_code = error_code
        if message is not None:
            self.message = message
        super(APIError, self).__init__(self.message)


class NotImplementedAPIError(APIError):
    status_code = 501
    error_code = 'not_implemented'
    message = _('Not implemented.')


class MethodNotAllowedAPIError(APIError):
    status_code = 405
    error_code = 'method_not_allowed'
    message = _('Method not allowed.')

    def __init__(self, methods=None, *args, **kwargs):
        if methods is not None:
            text =  _('Allowed methods:')
            self.message = '{0} {1} {2}'.format(self.message, text, ', '.join(methods))
        super(MethodNotAllowedAPIError, self).__init__(*args, **kwargs)


class BadRequestAPIError(APIError):
    status_code = 400
    error_code = 'bad_request'
    message = _('Bad request.')


class UnauthorizedAPIError(APIError):
    status_code = 401
    error_code = 'unauthorized'
    message = _('You need to log in.')
