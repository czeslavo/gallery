from django.db.models import Count
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView, FormView
from django.views.generic.detail import DetailView
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from ... import helpers
from ...forms.core import AddPhotoForm, EditPhotoForm, CommentForm
from ...models import CurrentPhoto, Photo, User
from ..mixins import LoginRequiredMixin
from ..paginator import paginate


def index(request):
    return render(request, "gallery/core/index.html")


class AddPhotoView(LoginRequiredMixin, FormView):
    template_name = 'gallery/core/add_photo.html'
    form_class = AddPhotoForm

    def form_valid(self, form):
        photo = form.save(commit=False)
        photo.user = self.request.user
        photo.save()
        return redirect(photo)


class RemovePhotoView(LoginRequiredMixin, TemplateView):
    template_name = 'gallery/core/remove_photo.html'

    def process_request(self):
        self.photo = Photo.objects \
            .filter(pk=self.kwargs['pk']) \
            .first()
        self.errors = []
        if not self.photo:
            self.errors.append(_('This photo does not exist.'))
        else:
            if self.request.user != self.photo.user:
                self.errors.append(_('This photo was not uploaded by you.'))

    def get_context_data(self, **kwargs):
        context = super(RemovePhotoView, self).get_context_data(**kwargs)
        context['photo'] = self.photo
        context['errors'] = self.errors
        return context

    def get(self, request, *args, **kwargs):
        self.process_request()
        return super(RemovePhotoView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.process_request()
        if not self.errors:
            self.photo.delete()
            return redirect('gallery:core:index')
        return super(RemovePhotoView, self).post(request, *args, **kwargs)


class EditPhotoView(LoginRequiredMixin, UpdateView):
    template_name = 'gallery/core/edit_photo.html'
    form_class = EditPhotoForm

    def get_object(self):
        return Photo.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        if self.request.user == self.object.user:
            form.save()
        return redirect(self.object)


class AddCurrentPhotoView(LoginRequiredMixin, TemplateView):
    template_name = 'gallery/core/add_current_photo.html'

    def get_context_data(self, **kwargs):
        context = super(AddCurrentPhotoView, self).get_context_data(**kwargs)
        context['photo'] = Photo.objects \
            .filter(pk=self.kwargs['pk']) \
            .first()
        context['current_photo'] = CurrentPhoto.objects \
            .filter(user=self.request.user, photo=context['photo'])\
            .first()
        return context


class RemoveCurrentPhotoView(LoginRequiredMixin, TemplateView):
    template_name = 'gallery/core/remove_current_photo.html'

    def process_request(self):
        self.current_photo = CurrentPhoto.objects \
            .filter(pk=self.kwargs['pk']) \
            .first()
        self.errors = []
        if not self.current_photo:
            self.errors.append(_('This photo does not exist.'))
        else:
            if self.request.user != self.current_photo.user:
                self.errors.append(_('This photo was not uploaded by you.'))

    def get_context_data(self, **kwargs):
        context = super(RemoveCurrentPhotoView, self).get_context_data(**kwargs)
        context['current_photo'] = self.current_photo
        context['errors'] = self.errors
        return context

    def get(self, request, *args, **kwargs):
        self.process_request()
        return super(RemoveCurrentPhotoView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.process_request()
        if not self.errors:
            self.current_photo.delete()
            return redirect(self.current_photo.photo)
        return super(RemoveCurrentPhotoView, self).post(request, *args, **kwargs)


class PhotoView(FormView):
    template_name = 'gallery/core/photo.html'
    form_class = CommentForm
    comments_per_page = 10
    current_photos_per_page = 5

    def get_photo(self):
        self.object = Photo.objects \
            .select_related('user') \
            .annotate(score=Count('photovote')) \
            .get(pk=self.kwargs['pk'])
        return self.object

    def get_your_current_photo(self):
        if self.request.user.is_authenticated():
            return CurrentPhoto.objects \
                .select_related('user', 'photo') \
                .annotate(score=Count('currentphotovote')) \
                .filter(user=self.request.user, photo=self.object) \
                .first()
        return None

    def get_comments(self):
        page = self.request.GET.get('comments_page')
        objects = self.object.comment_set \
            .select_related('user') \
            .order_by('-created') \
            .all()
        return paginate(objects, self.comments_per_page, page)

    def get_current_photos(self):
        page = self.request.GET.get('photos_page')
        objects = self.object.currentphoto_set \
            .select_related('user', 'photo') \
            .annotate(score=Count('currentphotovote')) \
            .order_by('-score') \
            .all()
        return paginate(objects, self.current_photos_per_page, page)

    def get_context_data(self, **kwargs):
        context = super(PhotoView, self).get_context_data(**kwargs)
        context['photo'] = self.get_photo()
        context['current_photos'] = self.get_current_photos()
        context['your_current_photo'] = self.get_your_current_photo()
        context['comments'] = self.get_comments()
        if 'compare' in self.kwargs:
            context['compare_to'] = CurrentPhoto.objects \
                .select_related('user', 'photo') \
                .annotate(score=Count('currentphotovote')) \
                .filter(photo=self.object, pk=self.kwargs['compare']) \
                .first()
            context['should_compare'] = True
        return context

    def form_valid(self, form):
        if self.request.user.is_authenticated():
            comment = form.save(commit=False)
            comment.user = self.request.user
            comment.photo = self.get_photo()
            comment.save()
        return redirect(self.get_photo())


class ProfileView(DetailView):
    model = User
    context_object_name = 'profile'

    def get_objects_per_page(self):
        return self.objects_per_page

    def get_context_objects_name(self):
        return self.context_objects_name

    def get_objects(self):
        raise NotImplementedError

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context[self.get_context_objects_name()] = paginate(
            self.get_objects(),
            self.get_objects_per_page(),
            self.request.GET.get('page')
        )
        return context


class ProfilePhotosView(ProfileView):
    template_name = 'gallery/core/profile_photos.html'
    objects_per_page = 15
    context_objects_name = 'photos'

    def get_objects(self):
        return self.object.photo_set.all()


class ProfileCurrentPhotosView(ProfileView):
    template_name = 'gallery/core/profile_current_photos.html'
    objects_per_page = 15
    context_objects_name = 'current_photos'

    def get_objects(self):
        return self.object.currentphoto_set.all()


class ProfileCommentsView(ProfileView):
    template_name = 'gallery/core/profile_comments.html'
    objects_per_page = 10
    context_objects_name = 'comments'

    def get_objects(self):
        return self.object.comment_set.all()


class ExploreView(TemplateView):
    template_name = 'gallery/core/explore.html'
    radius = 5 # km
    photos_per_page = 15

    def dispatch(self, *args, **kwargs):
        self.lon = float(self.kwargs.get('lon'))
        self.lat = float(self.kwargs.get('lat'))
        return super(ExploreView, self).dispatch(*args, **kwargs)

    def get_photos(self):
        lat_offset = helpers.get_lat_offset(self.radius)
        lon_offset = helpers.get_lon_offset(self.radius, self.lat)
        page = self.request.GET.get('page')
        objects = Photo.objects \
            .select_related('user') \
            .annotate(score=Count('photovote')) \
            .filter(lon__gt=self.lon - lon_offset, lon__lt=self.lon + lon_offset,
                    lat__gt=self.lat - lat_offset, lat__lt=self.lat + lat_offset) \
            .all()
        return paginate(objects, self.photos_per_page, page)

    def get_context_data(self, **kwargs):
        context = super(ExploreView, self).get_context_data(**kwargs)
        context['photos'] = self.get_photos()
        context['lon'] = self.lon
        context['lat'] = self.lat
        return context
