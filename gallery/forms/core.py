from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from ..models import CurrentPhoto, Photo, Comment


class AddPhotoForm(forms.ModelForm):

    class Meta:
        model = Photo
        fields = ['image', 'lat', 'lon', 'description']
        widgets = {
            'image': forms.ClearableFileInput(),
            'lat': forms.HiddenInput(),
            'lon': forms.HiddenInput(),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
        }


class EditPhotoForm(forms.ModelForm):

    class Meta:
        model = Photo
        fields = ['lat', 'lon', 'description']
        widgets = {
            'lat': forms.HiddenInput(),
            'lon': forms.HiddenInput(),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
        }


class CurrentImageField(forms.ImageField):

    content_types = ['image/png', 'image/jpeg']
    message = _('Invalid image type.')

    def __init__(self, *args, content_types=None, **kwargs):
        if content_types is not None:
            self.content_types = content_types
        super(CurrentImageField, self).__init__(*args, **kwargs)

    def to_python(self, data):
        f = super(CurrentImageField, self).to_python(data)
        if f is None:
            return None
        if not f.content_type in self.content_types:
            raise ValidationError(self.message)
        return f


class AddCurrentPhotoForm(forms.ModelForm):
    image = CurrentImageField()

    class Meta:
        model = CurrentPhoto
        fields = ['image']


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control'}),
        }
