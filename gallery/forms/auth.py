from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, \
    PasswordChangeForm as BuiltinPasswordChangeForm
from django.utils.translation import ugettext_lazy as _
from ..models import UserData


class RegisterForm(UserCreationForm):
    username = forms.CharField(
        label=_('Username'),
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )

    password1 = forms.CharField(
        label=_('Password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )

    password2 = forms.CharField(
        label=_('Password confirmation'),
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        strip=False,
        help_text=_('Enter the same password as before, for verification.'),
    )


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        label=_('Username'),
        max_length=254,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    password = forms.CharField(
        label=_('Password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )


class ProfileForm(forms.ModelForm):
    location_help_text = _('Your location is used to select the most '
                           'interesting photos from your area and can be '
                           'displayed in your profile if desired.')

    class Meta:
        model = UserData
        fields = ['about_me', 'display_location', 'lon', 'lat']
        widgets = {
            'about_me': forms.Textarea(attrs={'class': 'form-control'}),
            'lon': forms.HiddenInput(),
            'lat': forms.HiddenInput(),
        }


class PasswordChangeForm(BuiltinPasswordChangeForm):
    old_password = forms.CharField(
        label=_('Old password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )

    new_password1 = forms.CharField(
        label=_('New password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        help_text=password_validation.password_validators_help_text_html(),
    )

    new_password2 = forms.CharField(
        label=_('New password confirmation'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )
