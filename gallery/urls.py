from django.conf.urls import url, include
from .views import api, auth, core, files


urlpatterns = [
    url(r'^api/', include(api.urlpatterns, namespace='api')),
    url(r'^', include(auth.urlpatterns, namespace='auth')),
    url(r'^', include(core.urlpatterns, namespace='core')),
    url(r'^', include(files.urlpatterns, namespace='files')),
]
